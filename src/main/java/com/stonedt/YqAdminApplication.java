package com.stonedt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YqAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(YqAdminApplication.class, args);
	}

}
